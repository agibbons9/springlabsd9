package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springlabsd9Application {

	public static void main(String[] args) {
		SpringApplication.run(Springlabsd9Application.class, args);
	}

}
