package com.oreillyauto.controllers;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
// You can add preauth security to a class instead of single methods:
// @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
// Do not enable the preauth code above!
public class LoginController {

    @GetMapping(value = {"/login"})
    public String login() {
        return "login";
    }

    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    // or @PreAuthorize("hasRole('USER')")
    // or @PreAuthorize("hasRole('ADMIN')")
    @GetMapping(value = "/home")
    public String home() {
        return "home";
    }
    
}
