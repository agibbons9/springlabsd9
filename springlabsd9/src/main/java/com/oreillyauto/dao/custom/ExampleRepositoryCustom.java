package com.oreillyauto.dao.custom;

import java.util.List;

import com.oreillyauto.domain.Example;

public interface ExampleRepositoryCustom {    
    // Add your interface methods here (that ARE NOT CRUD or Spring Data 
    // methods [not concrete implementations])
    public void testQueries(String day);
    public List<Example> getExamples();
    public List<Example> getExampleById(Integer id);
    public List<Object> getTables();
}

